// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "GameFramework/PlayerController.h"
#include "TankPlayerController.generated.h"

class ATank;
class UTankTurret;

/**
 * 
 */
UCLASS()
class BATTLETANK_API ATankPlayerController : public APlayerController
{
	GENERATED_BODY()
	
public:
	ATank* GetControlledTank() const;
	
	void BeginPlay() override;

	void Tick(float DeltaTime) override;

private:

	void AimTowardsCrosshair();

	bool GetSightHitLocation(FVector& OutHitLocation) const;

	bool GetLookDirection(FVector2D ScreenLocation, FVector& LookDirection) const;

	bool GetLookVectorHitLocation(FVector LookDirection, FVector& HitLocation) const;

	UPROPERTY(EditDefaultsOnly)
	float CrosshairXLocation = 0.5f; // set manually. needs to match UI Widget's crosshair location on the screen

	UPROPERTY(EditDefaultsOnly)
	float CrosshairYLocation = 0.3f; // set manually. needs to match UI Widget's crosshair location on the screen

	UPROPERTY(EditDefaultsOnly) // line trace max lenght
	float LineRange = 5000000.f;

};
