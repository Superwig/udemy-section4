// Fill out your copyright notice in the Description page of Project Settings.

#include "TankAimingComponent.h"
#include "Runtime/Engine/Classes/GameFramework/Actor.h"
#include "TankBarrel.h"
#include "TankTurret.h"
#include "Runtime/Engine/Classes/Kismet/GameplayStatics.h"

// Sets default values for this component's properties
UTankAimingComponent::UTankAimingComponent()
{
	// Set this component to be initialized when the game starts, and to be ticked every frame.  You can turn these features
	// off to improve performance if you don't need them.
	PrimaryComponentTick.bCanEverTick = true; // should this component tick?

	// ...
}


// Called when the game starts
void UTankAimingComponent::BeginPlay()
{
	Super::BeginPlay();
	// ...
	
}


// Called every frame
void UTankAimingComponent::TickComponent(float DeltaTime, ELevelTick TickType, FActorComponentTickFunction* ThisTickFunction)
{
	Super::TickComponent(DeltaTime, TickType, ThisTickFunction);

	// ...
}

void UTankAimingComponent::AimAt(FVector HitLocation, float LaunchSpeed) {
	if (!BarrelMesh) { return; }

	FVector OutLaunchVelocity(0); // always initialize as: x=0, y=0, z=0
	FVector StartLocation = BarrelMesh->GetSocketLocation(FName("ProjectileSpawn"));

	if (UGameplayStatics::SuggestProjectileVelocity( // if sucessfully calculates aim solution then:
		this,
		OutLaunchVelocity,
		StartLocation,
		HitLocation,
		LaunchSpeed,
		false,
		0,
		0,
		ESuggestProjVelocityTraceOption::DoNotTrace)) {

		auto AimDirection = OutLaunchVelocity.GetSafeNormal();
		MoveBarrelTowards(AimDirection);
	}
	else { // if can't calculate aim solution then:
		auto AimDirection = FVector(0, 0, -1);
		MoveBarrelTowards(AimDirection);
	}

}

void UTankAimingComponent::SetBarrelReference(UTankBarrel* BarrelToSet) {
	BarrelMesh = BarrelToSet;
}

void UTankAimingComponent::SetTurretReference(UTankTurret* TurretToSet) {
	TurretMesh = TurretToSet;
}

void UTankAimingComponent::MoveBarrelTowards(FVector AimDirection) {
	//calculate difference between current rotation and aim direction
	auto BarrelRotator = BarrelMesh->GetForwardVector().Rotation();
	auto AimAsRotator = AimDirection.Rotation();
	auto DeltaRotator = AimAsRotator - BarrelRotator;
	BarrelMesh->ElevateBarrel(DeltaRotator.Pitch);
	TurretMesh->RotateTurret(DeltaRotator.Yaw);
}

void UTankAimingComponent::MoveTurretTowards(FVector AimDirection) {
	//calculate difference between current rotation and aim direction
	auto TurretRotator = TurretMesh->GetForwardVector().Rotation().Yaw;
	auto AimAsRotator = AimDirection.Rotation().Yaw;
	auto DeltaRotator = AimAsRotator - TurretRotator;
	TurretMesh->RotateTurret(DeltaRotator);
}

