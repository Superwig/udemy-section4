// Fill out your copyright notice in the Description page of Project Settings.

#include "TankBarrel.h"

void UTankBarrel::ElevateBarrel(float RelativeSpeed) {
	auto ClampedRelativeSpeed = FMath::Clamp<float>(RelativeSpeed, -1, 1);
	auto ElevationChange = ClampedRelativeSpeed * MaxDegreesPerSecond * GetWorld()->DeltaTimeSeconds;
	auto RawNewElevation = RelativeRotation.Roll - ElevationChange; //Because of used tank model needs to use RelativeRotation.Roll and not RelativeRotation.Pitch
	auto ClampedElevation = FMath::Clamp<float>(RawNewElevation, MinimumElevation, MaximumElevation);

	SetRelativeRotation(FRotator(0, 0, ClampedElevation));
}


