// Fill out your copyright notice in the Description page of Project Settings.

#include "TankTurret.h"


void UTankTurret::RotateTurret(float RelativeSpeed) {
	auto AimReferenceRotation = AimReference->RelativeRotation.Yaw;
	auto CurrentRotation = RelativeRotation;
	FRotator DesiredYawRotation = FRotator(0, AimReferenceRotation - CompensateForRotation, 0);
	auto NewRotation = FMath::RInterpTo(CurrentRotation, DesiredYawRotation, GetWorld()->DeltaTimeSeconds, InterpolateSpeed);
	SetRelativeRotation(NewRotation);
}

// BlueprintCallable to set reference in Event Begin Play
void UTankTurret::SetAimReference(USceneComponent* GimbleReference) {
	AimReference = GimbleReference;
}

