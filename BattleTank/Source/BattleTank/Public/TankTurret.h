// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankTurret.generated.h"

/**
 * 
 */

UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankTurret : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:

	void RotateTurret(float RelativeSpeed);

	UFUNCTION(BlueprintCallable, Category = Setup)
	void SetAimReference(USceneComponent* GimbleReference);

private:

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float InterpolateSpeed = 1.f;

	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float CompensateForRotation = 90.f;

	USceneComponent* AimReference = nullptr;

};
