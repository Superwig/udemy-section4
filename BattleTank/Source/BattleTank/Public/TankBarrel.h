// Fill out your copyright notice in the Description page of Project Settings.

#pragma once

#include "CoreMinimal.h"
#include "Components/StaticMeshComponent.h"
#include "TankBarrel.generated.h"

/**
 * 
 */
UCLASS(ClassGroup = (Custom), meta = (BlueprintSpawnableComponent))
class BATTLETANK_API UTankBarrel : public UStaticMeshComponent
{
	GENERATED_BODY()
	
public:
	// -1 is downward movement and +1 is upward movement
	void ElevateBarrel(float RelativeSpeed);
	
private:
	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float MaxDegreesPerSecond = 5.f;

	// because of used tank model, value needs to be inverted. Suggested 0
	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float MaximumElevation = 40.f;

	// because of used tank model, value needs to be inverted. Suggested -40
	UPROPERTY(EditDefaultsOnly, Category = Setup)
	float MinimumElevation = -10.f;
};
